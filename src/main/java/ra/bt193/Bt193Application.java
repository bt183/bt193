package ra.bt193;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bt193Application {

    public static void main(String[] args) {
        SpringApplication.run(Bt193Application.class, args);
    }

}
